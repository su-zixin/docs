# 统计信息收集<a name="ZH-CN_TOPIC_0311524275"></a>

对外表进行查询时，尤其关联、汇聚等复杂查询时，为了保证能获得更优的性能，建议定期对外表执行Analyze以更新统计信息

## 操作步骤<a name="zh-cn_topic_0085032190_zh-cn_topic_0059779302_section97581768562"></a>

1.  使用ANALYZE语句更新统计信息。

    ```
    postgres=# ANALYZE customer;
    ANALYZE
    ```

    使用ANALYZE VERBOSE语句更新统计信息，并输出表的相关信息。

    ```
    postgres=# ANALYZE VERBOSE customer;
    ANALYZE
    ```

    也可以同时执行VACUUM ANALYZE命令进行查询优化。

    ```
    postgres=# VACUUM ANALYZE customer;
    VACUUM
    ```

2.  删除表

    ```
    postgres=# DROP TABLE customer;
    postgres=# DROP TABLE customer_par;
    postgres=# DROP TABLE part;
    ```

    当结果显示为如下信息，则表示删除成功。

    ```
    DROP TABLE
    ```


