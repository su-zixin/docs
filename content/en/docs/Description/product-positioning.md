# Product Positioning<a name="EN-US_TOPIC_0289896521"></a>

openGauss is a HA rational database that supports the SQL2003 standard and primary/standby deployment.

-   Multiple storage modes support composite service scenarios.
-   The NUMA data structure supports high performance.
-   Primary/standby deployment and CRC support HA.

openGauss is a multi-core-oriented open-source relational database that provides ultimate performance, full-link service and data security, AI-based tuning, and efficient O&M capabilities. This leading database at enterprise level is developed in collaboration with global partners and is released under the Mulan Permissive Software License v2. openGauss deeply integrates Huawei's years of R&D experience in the database field and continuously builds competitive features based on enterprise-level scenario requirements.

