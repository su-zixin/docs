# Enterprise-Level Features<a name="EN-US_TOPIC_0000001135548517"></a>

-   **[Support for Functions and Stored Procedures](support-for-functions-and-stored-procedures.md)**  

-   **[SQL Hints](sql-hints.md)**  

-   **[Full-Text Indexing](full-text-indexing.md)**  

-   **[Copy Interface for Error Tolerance](copy-interface-for-error-tolerance.md)**  

-   **[Partitioning](partitioning.md)**  

-   **[Support for Advanced Analysis Functions](support-for-advanced-analysis-functions.md)**  

-   **[Materialized View](materialized-view.md)**  

-   **[HyperLogLog](hyperloglog.md)**  

-   **[Creating an Index Online](creating-an-index-online.md)**  

-   **[Autonomous Transaction](autonomous-transaction.md)**  

-   **[Global Temporary Table](global-temporary-table.md)**  

-   **[Global Partition Index](global-partition-index.md)**  

-   **[AI Capabilities](ai-capabilities.md)**  

-   **[Pseudocolumn ROWNUM](pseudocolumn-rownum.md)**  

-   **[Stored Procedure Debugging](stored-procedure-debugging.md)**  


