# Miscellaneous Parameters<a name="EN-US_TOPIC_0289900522"></a>

## server\_version<a name="en-us_topic_0283137574_en-us_topic_0237124754_en-us_topic_0059778487_s1df9119d74fe45da9452d4cb4802f84c"></a>

**Parameter description**: Specifies the server version number.

This parameter is an INTERNAL parameter. The value of this parameter cannot be modified.

**Value range**: a string

**Default value**:  **9.2.4**

## server\_version\_num<a name="en-us_topic_0283137574_en-us_topic_0237124754_en-us_topic_0059778487_sa4182f08e006431fbad639fe6963560f"></a>

**Parameter description**: Specifies the server version number.

This parameter is an INTERNAL parameter. The value of this parameter cannot be modified.

**Value range**: an integer

**Default value**:  **90204**

## block\_size<a name="en-us_topic_0283137574_en-us_topic_0237124754_en-us_topic_0059778487_s46449cae21604e8d828a3614d26d3874"></a>

**Parameter description**: Specifies the block size of the current database.

This parameter is an INTERNAL parameter. The value of this parameter cannot be modified.

**Value**:  **8192**

**Default value**:  **8192**

## segment\_size<a name="en-us_topic_0283137574_en-us_topic_0237124754_en-us_topic_0059778487_se61b24947cbf4fc99dbf52bbdbd5291b"></a>

**Parameter description**: Specifies the segment file size of the current database.

This parameter is an INTERNAL parameter. The value of this parameter cannot be modified.

**Unit**: 8 KB

**Default value**: 131072, that is, 1 GB

## max\_index\_keys<a name="en-us_topic_0283137574_en-us_topic_0237124754_en-us_topic_0059778487_s1992e0f4a9694daba20844fd94408f80"></a>

**Parameter description**: Specifies the maximum number of index keys supported by the current database.

This parameter is an INTERNAL parameter. The value of this parameter cannot be modified.

**Default value**:  **32**

## integer\_datetimes<a name="en-us_topic_0283137574_en-us_topic_0237124754_en-us_topic_0059778487_s2cf7071ca7cf4b79a23cbc4664f508a8"></a>

**Parameter description**: Specifies whether the date and time are in the 64-bit integer format.

This parameter is an INTERNAL parameter. The value of this parameter cannot be modified.

**Value range**: Boolean

-   **on**  indicates that the 64-bit integer format is used.
-   **off**  indicates that the 64-bit integer format is not used.

**Default value**:  **on**

## lc\_collate<a name="en-us_topic_0283137574_en-us_topic_0237124754_en-us_topic_0059778487_s2f3ca5fe93c04242aae028fd44ffb57c"></a>

**Parameter description:**  Specifies the locale in which sorting of textual data is done.

This parameter is an INTERNAL parameter. The value of this parameter cannot be modified.

**Default value**: Determined by the configuration set during the openGauss installation and deployment.

## lc\_ctype<a name="en-us_topic_0283137574_en-us_topic_0237124754_en-us_topic_0059778487_s8d813413a667463db959fd155dca4a7d"></a>

**Parameter description**: Specifies the locale that determines character classifications. For example, it specifies what a letter and its upper-case equivalent are.

This parameter is an INTERNAL parameter. The value of this parameter cannot be modified.

**Default value**: Determined by the configuration set during the openGauss installation and deployment.

## max\_identifier\_length<a name="en-us_topic_0283137574_en-us_topic_0237124754_en-us_topic_0059778487_s0aed6ba352a6486a9f2065914e6322c4"></a>

**Parameter description**: Specifies the maximum identifier length.

This parameter is an INTERNAL parameter. The value of this parameter cannot be modified.

**Value range**: an integer

**Default value**:  **63**

## server\_encoding<a name="en-us_topic_0283137574_en-us_topic_0237124754_en-us_topic_0059778487_s6ea4fdcca287481ba5fff4d6defeaf79"></a>

**Parameter description**: Specifies the database encoding \(character set\).

By default, gs\_initdb will initialize the setting of this parameter based on the current system environment. You can also run the  **locale**  command to check the current configuration environment.

This parameter is an INTERNAL parameter. The value of this parameter cannot be modified.

**Default value:**  determined by the current system environment when the database is created.

## enable\_upgrade\_merge\_lock\_mode<a name="en-us_topic_0283137574_en-us_topic_0237124754_en-us_topic_0059778487_s46dd96b9ae0c45ff83bb1c444cbc4327"></a>

**Parameter description**: If this parameter is set to  **on**, the delta merge operation internally increases the lock level, and errors can be prevented when update and delete operations are performed at the same time.

This parameter is a USERSET parameter. Set it based on instructions provided in  [Table 1](en-us_topic_0289899927.md#en-us_topic_0283137176_en-us_topic_0237121562_en-us_topic_0059777490_t91a6f212010f4503b24d7943aed6d846).

**Value range**: Boolean

-   If this parameter is set to  **on**, the delta merge operation internally increases the lock level. In this way, when the  **DELTAMERGE**  operation is concurrently performed with the  **UPDATE**  or  **DELETE**  operation, one operation can be performed only after the previous one is complete.
-   If this parameter is set to  **off**  and the  **DELTAMERGE**  operation is concurrently performed with the  **UPDATE**  or  **DELETE**  operation to the data in a row in the delta table of the table, errors will be reported during the later operation, and the operation will stop.

**Default value**:  **off**

## transparent\_encrypted\_string<a name="en-us_topic_0283137574_en-us_topic_0237124754_section59019117496"></a>

**Parameter description**: Specifies a sample string that is transparently encrypted. Its value is generated by encrypting  **TRANS\_ENCRYPT\_SAMPLE\_STRING**  using a database secret key. The ciphertext is used to check whether the DEK obtained during secondary startup is correct. If it is incorrect, database nodes will not be started. This parameter is a POSTMASTER parameter. Set it based on instructions provided in  [Table 1](en-us_topic_0289899927.md#en-us_topic_0283137176_en-us_topic_0237121562_en-us_topic_0059777490_t91a6f212010f4503b24d7943aed6d846). This parameter applies only to the DWS scenario in the current version.

**Value range**: a string. An empty string indicates that openGauss is a not encrypted.

**Default value**: empty

>![](public_sys-resources/icon-note.gif) **NOTE:** 
>Do not set this parameter manually. Otherwise, openGauss may become faulty.

## transparent\_encrypt\_kms\_url<a name="en-us_topic_0283137574_en-us_topic_0237124754_section14139346195718"></a>

**Parameter description**: Specifies the URL for obtaining the database secret key to be transparently encrypted. It must contain only the characters specified in RFC3986, and the maximum length is 2047 bytes. The format is  **kms://**_Protocol_**@**_KMS host name 1_**;**_KMS host name 2_**:**_KMS port number_**/kms**, for example,  **kms://https@linux175:29800/**. This parameter applies only to the DWS scenario in the current version.

This parameter is a POSTMASTER parameter. Set it based on instructions provided in  [Table 1](en-us_topic_0289899927.md#en-us_topic_0283137176_en-us_topic_0237121562_en-us_topic_0059777490_t91a6f212010f4503b24d7943aed6d846).

**Value range**: a string

**Default value**: empty

## transparent\_encrypt\_kms\_region<a name="en-us_topic_0283137574_en-us_topic_0237124754_section11856132918597"></a>

**Parameter description**: Specifies the deployment region of openGauss. It must contain only the characters specified in RFC3986, and the maximum length is 2047 bytes. This parameter applies only to the DWS scenario in the current version.

This parameter is a POSTMASTER parameter. Set it based on instructions provided in  [Table 1](en-us_topic_0289899927.md#en-us_topic_0283137176_en-us_topic_0237121562_en-us_topic_0059777490_t91a6f212010f4503b24d7943aed6d846).

**Value range**: a string

**Default value**: empty

## basebackup\_timeout<a name="section2190024353"></a>

**Parameter description:**  Specifies the timeout interval for a connection that has no read or write operations after a backup transfer is complete.

When the gs\_basebackup tool is used for transmission and a high compression rate is specified, the transmission of the tablespace may time out \(the client needs to compress the transmitted data\).

**Value range**: an integer ranging from 0 to  _INT\_MAX_. The unit is s.  **0**  indicates that archiving timeout is disabled.

**Default value:**  600s

## datanode\_heartbeat\_interval<a name="en-us_topic_0283137574_en-us_topic_0237124754_section136882143238"></a>

**Parameter description**: Specifies the interval at which heartbeat messages are sent between heartbeat threads. You are advised to set this parameter to a value no more than wal\_receiver\_timeout/2.

This parameter is a SIGHUP parameter. Set it based on instructions provided in  [Table 1](en-us_topic_0289899927.md#en-us_topic_0283137176_en-us_topic_0237121562_en-us_topic_0059777490_t91a6f212010f4503b24d7943aed6d846).

**Value range**: an integer ranging from 1000 to 60000. The unit is ms.

Default value:  **1s**

