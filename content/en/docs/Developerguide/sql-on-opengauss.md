# SQL on openGauss<a name="EN-US_TOPIC_0311524280"></a>

-   **[Overview](overview-3.md)**  

-   **[Data Processing Based on Extension Connector](data-processing-based-on-extension-connector.md)**  

-   **[Data Processing Based on Foreign Tables](data-processing-based-on-foreign-tables.md)**  


